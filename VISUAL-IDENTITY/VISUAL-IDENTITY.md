# Le Grand Tour des Photinias – Visual Identity
---

## Color Pallet
Official color pallet is composed of five main colors. Black and white have been added due to the recurrent use of them.

<span style="background-color: #FFD034">&emsp;&ensp;</span>
&emsp;Web: #FFD034

<span style="background-color: #06327D">&emsp;&ensp;</span>
&emsp;Web: #06327D

<span style="background-color: #209D5C">&emsp;&ensp;</span>
&emsp;Web: #209D5C

<span style="background-color: #FFF">&emsp;&ensp;</span>
&emsp;Web: #FFFFFF

<span style="background-color: #000">&emsp;&ensp;</span>
&emsp;Web: #000000

---
## Typography
The typeface for the official logo is Archivo Black. This font is generally used in all caps format, which is perfectly appropriate for our use (logo, titles and subtitles).
Body's typeface is Evolve Sans. Really useful with its different font weight alternatives.

---
## Official Logo
The official logo is as follow. The bike wheel is important for us. It represent the passion of biking but also the circle we are triyng to follow. As we started this event together, the two bikes represent us.

![Official Logo](./logotype/bg-color/Logo1.0.svg)

It is also available with transparent backgroung.

![Official Logo](./logotype/bg-transparent/Logo1.0.svg)

---
## Alternative Logo
Different alternative logo can be use with the color pallet, colored or not.

### Black and white
![Official Logo](./logotype/bg-color/LogoB&W.svg)
![Official Logo](./logotype/bg-transparent/LogoB&W.svg)

### Colored
![Official Logo](./logotype/bg-color/Logo2.1.svg)
![Official Logo](./logotype/bg-transparent/Logo2.1.svg)

![Official Logo](./logotype/bg-color/Logo2.2.svg)
![Official Logo](./logotype/bg-transparent/Logo2.2.svg)

![Official Logo](./logotype/bg-color/Logo2.3.svg)
![Official Logo](./logotype/bg-transparent/Logo2.3.svg)

![Official Logo](./logotype/bg-color/Logo2.4.svg)
