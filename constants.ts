export const publicSessionToken = "0da1e9f6c33ea71fa51e7c5fad11f693";

export const iframeUrl =
  "https://nextcloud.app.simonwork.fr/apps/phonetrack/publicSessionWatch/0da1e9f6c33ea71fa51e7c5fad11f693?lineToggle=1&refresh=15&arrow=0&gradient=0&autozoom=1&tooltip=0&linewidth=4&pointradius=8&nbpoints=1000";

export const startDate = new Date("Apr 24 2021 8:30:00 GMT+0200");

export const endDate = new Date("Apr 24 2021 16:34:10 GMT+0200");