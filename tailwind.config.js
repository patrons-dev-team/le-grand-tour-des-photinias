module.exports = {
  purge: ["./index.html", "./src/**/*.{vue,js,ts,jsx,tsx}"],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      backgroundImage: () => ({
        logo: "url('/public/Logo2.3.svg')",
        texture: "url('/bg.jpg')",
        "image-1": "url('/picture-1.jpg')",
        "image-2": "url('/picture-2.jpg')",
      }),
      spacing: {
        144: "36rem",
        160: "40rem",
        176: "44rem",
        192: "48rem",
      },
      colors: {
        yellow: {
          photinias: "#FFD034",
        },
        blue: {
          photinias: "#06327D",
          dark: "#0C032E",
        },
        green: {
          photinias: "#209D5C",
        },
        gray: {
          dark: "#2B2933",
        },
      },
      zIndex: {
        "-10": "-10",
        "-20": "-20",
      },
      fontFamily: {
        title: ['"Archivo Black"'],
        body: ["Roboto"],
      },
    },
  },
  variants: {
    extend: {
      fontSize: ["rfs"],
      padding: ["rfs"],
      margin: ["rfs"],
      gap: ["rfs"],
      width: ["rfs"],
      height: ["rfs"],
      inset: ["rfs"],
    },
  },
  plugins: [require("tailwindcss-rfs")],
};
