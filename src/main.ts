import { ViteSSG } from "vite-ssg";
import generatedRoutes from "virtual:generated-pages";
import lazyPlugin from 'vue3-lazy'
import { setupLayouts } from "layouts-generated";
import App from "./App.vue";
import 'typeface-archivo-black';
import 'typeface-roboto';
import "./styles/main.css";
const routes = setupLayouts(generatedRoutes);

// https://github.com/antfu/vite-ssg
export const createApp = ViteSSG(App, { routes }, (ctx) => {
  // install all modules under `modules/`
  Object.values(import.meta.globEager("./modules/*.ts")).map((i) =>
    i.install?.(ctx)
  );
  ctx.app.use(lazyPlugin, {})
});
