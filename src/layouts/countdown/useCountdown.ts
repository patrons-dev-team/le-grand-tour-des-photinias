import { reactive, ref, SetupContext } from "vue";
import intervalToDuration from "date-fns/intervalToDuration";
import util from "./util";
export type textObject = {
  remainingSeconds: { value: number; text: string };
  remainingMinutes: { value: number; text: string };
  remainingHours: { value: number; text: string };
  remainingDays: { value: number; text: string };
  remainingWeeks: { value: number; text: string };
  remainingMonths: { value: number; text: string };
  remainingYears: { value: number; text: string };
};
export default function (
  props: Readonly<
    {
      startDateTime: Date;
      endDateTime: Date;
      startImmediately: boolean;
      tickDelay: number;
    } & {}
  >,
  context: SetupContext<Record<string, any>>
) {
  const countdown = reactive({
    remainingSeconds: { value: 0, text: "" },
    remainingMinutes: { value: 0, text: "" },
    remainingHours: { value: 0, text: "" },
    remainingDays: { value: 0, text: "" },
    remainingWeeks: { value: 0, text: "" },
    remainingMonths: { value: 0, text: "" },
    remainingYears: { value: 0, text: "" },
  });

  const isRunning = ref(false);

  let tickTimeout: ReturnType<typeof setTimeout>;

  const run = async () => {
    if (!isRunning.value) {
      context.emit("start");
    }

    isRunning.value = true;

    const startDateTime = props.startDateTime
      ? props.startDateTime
      : new Date();
    if (startDateTime >= props.endDateTime) {
      context.emit("finished");
      isRunning.value = false;
      return;
    }

    const result = intervalToDuration({
      start: startDateTime,
      end: props.endDateTime,
    });

    // date-fns doesn't calculate weeks for us.
    // we can calculate it ourselves though
    let days = result.days == undefined ? 0 : result.days;
    result.weeks = Math.floor(days / 7);

    // Since we've calculated the weeks, we need to adjust
    // the day count
    days %= 7;

    Object.keys(result).forEach((key: string) => {
      const keyName = `remaining${util.capitaliseFirstletter(key)}`;

      const value = (result[key as keyof Duration] === undefined
        ? 0
        : result[key as keyof Duration]) as number;
      const text = util.getFormatString(
        value,
        key.substring(0, key.length - 1)
      );
      countdown[keyName as keyof typeof countdown].value = value;
      countdown[keyName as keyof typeof countdown].text = text;
    });

    // Catchall to prevent a race condition where the countdown
    // calculation happens just when the count finishes.
    if (util.getHasCountdownFinished(countdown)) {
      context.emit("finished");
      isRunning.value = false;
      return;
    }
    context.emit("tick", countdown);

    tickTimeout = setTimeout(() => {
      run();
    }, props.tickDelay);
  };

  const cancel = () => {
    clearTimeout(tickTimeout);
    isRunning.value = false;
    context.emit("cancelled");
  };

  return {
    countdown,
    run,
    cancel,
    isRunning,
  };
}
