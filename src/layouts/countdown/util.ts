import { textObject } from "./useCountdown";

function getHasCountdownFinished(data: textObject) {
  // eslint-disable-next-line no-param-reassign
  const sum = Object.values(data).reduce(
    (acc, current) => (acc += current.value),
    0
  );

  return sum === 0;
}

function getFormatString(value: number, type: string): string {
  const prefix = `${type}${value === 1 ? "" : "s"}`;
  return `${value} ${prefix}`;
}

function capitaliseFirstletter(word: string): string {
  return `${word.substring(0, 1).toUpperCase()}${word.substring(1)}`;
}

export default {
  getHasCountdownFinished,
  getFormatString,
  capitaliseFirstletter,
};
